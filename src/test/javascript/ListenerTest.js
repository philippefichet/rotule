define(["Router", "RouteListener"], function(Router, RouteListener) {
    describe("Router Listener Tests", function() {
        it("startLoadingRoute", function() {
            var start = false;
            var route = false;
            Router.default.baseUrl = "";
            Router.default.addListener("testListenerStart", new RouteListener.default({
                startLoadingRoute: function() {
                    start = true;
                }
            }))
            Router.default.addNewRoute("/testListenerStart", function() {
                route = true;
            });
            Router.default.handleRoute("/testListenerStart", false);
            Router.default.removeListener("testListenerStart");
            expect(start).toBe(true);
            expect(route).toBe(true);
        });
        it("endLoadingRoute", function() {
            var end = false;
            var route = false;
            Router.default.baseUrl = "";
            Router.default.addListener("testListenerEnd", new RouteListener.default({
                endLoadingRoute: function() {
                    end = true;
                }
            }))
            Router.default.addNewRoute("/testListenerEnd", function() {
                route = true;
            });
            Router.default.handleRoute("/testListenerEnd", false);
            Router.default.removeListener("testListenerEnd");
            expect(end).toBe(true);
            expect(route).toBe(true);
        });
    });
});