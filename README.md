# Rotule

An experimental small routing between URL and page action

## Example with requirejs

```js
requirejs(["Router"], function (router) {
    router.default.addNewRoute("/account", function () {
        // Show account page with ajax, reactjs, angularjs, ...
    })

    // handle current url
    router.default.handleRoute(document.location.pathname);
})
```

### Automatique link with jQuery
```js
requirejs(["jquery", "Router"] , function($, router) {
    $(document).on("click", "a", function (event) {
        var $this = $(this);
        var href = $this.attr("href");
        router.default.handleRoute(href);
        return false;
    });
});
```