define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Route = (function () {
        function Route() {
            this.title = null;
            this.module = false;
        }
        return Route;
    }());
    exports["default"] = Route;
});
//# sourceMappingURL=Route.js.map