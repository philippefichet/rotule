export default class Route {
    load: Function
    title: string = null
    path: string
    module: boolean = false
}
