define(["require", "exports", "Route"], function (require, exports, Route_1) {
    "use strict";
    exports.__esModule = true;
    var Router = (function () {
        function Router() {
            this.baseUrl = "/";
            this.routes = [];
            this.eventListener = {};
        }
        Router.prototype.fireStartChangeRouting = function (path) {
            console.log(this.eventListener);
            for (var name_1 in this.eventListener) {
                var listener = this.eventListener[name_1];
                if (listener.startLoadingRoute !== null) {
                    listener.startLoadingRoute(path);
                }
            }
        };
        Router.prototype.fireEndChangeRouting = function (path) {
            for (var name_2 in this.eventListener) {
                var listener = this.eventListener[name_2];
                if (listener.endLoadingRoute !== null) {
                    listener.endLoadingRoute(path);
                }
            }
        };
        Router.prototype.addListener = function (name, listener) {
            this.eventListener[name] = listener;
        };
        Router.prototype.removeListener = function (name) {
            var listener = this.eventListener[name];
            delete this.eventListener[name];
            return listener;
        };
        Router.prototype.addNewRoute = function (path, load) {
            var route = new Route_1["default"]();
            route.path = path;
            route.load = load;
            this.routes.push(route);
            return route;
        };
        Router.prototype.addNewRouteRequireJS = function (path, modules, load) {
            var _this = this;
            var route = this.addNewRoute(path, function () {
                var requirejs = window["requirejs"];
                requirejs(modules, function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    load.apply(_this, args);
                    _this.fireEndChangeRouting(path);
                });
            });
            route.module = true;
            return route;
        };
        Router.prototype.handleRoute = function (url, pushState) {
            if (pushState === void 0) { pushState = true; }
            var baseUrl = url.replace(this.baseUrl.substring(1), "");
            this.fireStartChangeRouting(baseUrl);
            var urlSplit = baseUrl.split("/");
            var routes = this.routes;
            var routeChoosen = null;
            var routeParameters = [];
            for (var i = 0; i < routes.length; i++) {
                var route = routes[i];
                var routeSplit = route.path.split("/");
                if (routeSplit.length === urlSplit.length) {
                    var selected = true;
                    var selectedParameters = [];
                    for (var j = 0; j < routeSplit.length; j++) {
                        var isParameter = (routeSplit[j].indexOf("{") === 0);
                        if (routeSplit[j] != urlSplit[j] && isParameter === false) {
                            selected = false;
                            break;
                        }
                        if (isParameter) {
                            selectedParameters.push(urlSplit[j]);
                        }
                    }
                    if (selected) {
                        routeChoosen = route;
                        routeParameters = selectedParameters;
                    }
                }
            }
            if (routeChoosen === null) {
                this.fireEndChangeRouting(baseUrl);
                throw Error("No route found");
            }
            if (typeof history.pushState === "undefined" && pushState !== false) {
                throw Error("Pushstate not available");
            }
            var title = routeChoosen.title != null ? routeChoosen.title : "";
            var state = { "url": url, "title": title };
            routeChoosen.load.apply(null, routeParameters);
            if (routeChoosen.module == false) {
                this.fireEndChangeRouting(baseUrl);
            }
            if (pushState !== false && document.location.pathname !== url) {
                history.pushState(state, title, url);
            }
        };
        return Router;
    }());
    var router = new Router();
    exports["default"] = router;
});
//# sourceMappingURL=Router.js.map