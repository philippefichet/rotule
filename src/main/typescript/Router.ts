import Route from "Route"
import RouteListener from "RouteListener"

class Router {
    protected baseUrl: string = "/"
    protected routes: Route[] = []
    protected eventListener: Object = {}
    private fireStartChangeRouting(path: String) {
        console.log(this.eventListener)
        for (let name in this.eventListener) {
            let listener: RouteListener = this.eventListener[name];
            if (listener.startLoadingRoute !== null) {
                listener.startLoadingRoute(path)
            }
        }
    }
    private fireEndChangeRouting(path: String) {
        for (let name in this.eventListener) {
            let listener: RouteListener = this.eventListener[name];
            if (listener.endLoadingRoute !== null) {
                listener.endLoadingRoute(path)
            }
        }
    }
    addListener(name: string, listener: RouteListener) {
        this.eventListener[name] = listener;
    }
    removeListener(name: string) : RouteListener {
        let listener: RouteListener = this.eventListener[name];
        delete this.eventListener[name];
        return listener;
    }
    addNewRoute(path: string, load: Function): Route {
        let route: Route = new Route();
        route.path = path;
        route.load = load;
        this.routes.push(route);
        return route;
    }
    addNewRouteRequireJS(path: string, modules: String[], load: Function): Route {
        let route = this.addNewRoute(path, () => {
            let requirejs: Function = window["requirejs"];
            requirejs(modules, (...args) => {
                load.apply(this, args);
                this.fireEndChangeRouting(path);
            })
        });
        route.module = true;
        return route;
    }
    handleRoute(url: string, pushState: boolean = true) {
        let baseUrl = url.replace(this.baseUrl.substring(1), "");
        this.fireStartChangeRouting(baseUrl);
        let urlSplit = baseUrl.split("/");
        let routes: Route[] = this.routes;
        let routeChoosen: Route = null;
        let routeParameters = [];
        for (let i = 0; i < routes.length; i++) {
            let route: Route = routes[i];
            let routeSplit = route.path.split("/");
            if (routeSplit.length === urlSplit.length) {
                let selected = true;
                let selectedParameters = [];
                for (let j = 0; j < routeSplit.length; j++) {
                    let isParameter = (routeSplit[j].indexOf("{") === 0);
                    if (routeSplit[j] != urlSplit[j] && isParameter === false) {
                        selected = false;
                        break;
                    }
                    if (isParameter) {
                        selectedParameters.push(urlSplit[j]);
                    }
                }
                if (selected) {
                    routeChoosen = route;
                    routeParameters = selectedParameters;
                }
            }
        }

        if (routeChoosen === null) {
            this.fireEndChangeRouting(baseUrl);
            throw Error("No route found");
        }

        if (typeof history.pushState === "undefined" && pushState !== false) {
            throw Error("Pushstate not available");
        }
        let title = routeChoosen.title != null ? routeChoosen.title : "";
        let state = {"url": url, "title": title};
        routeChoosen.load.apply(null, routeParameters);
        if (routeChoosen.module == false) {
            this.fireEndChangeRouting(baseUrl);
        }
        if (pushState !== false && document.location.pathname !== url) {
            history.pushState(state, title, url);
        }
    }
}

let router = new Router();

export default router