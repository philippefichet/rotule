define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var RouteListener = (function () {
        function RouteListener(options) {
            this.startLoadingRoute = null;
            this.endLoadingRoute = null;
            if (options["startLoadingRoute"] !== undefined) {
                this.startLoadingRoute = options["startLoadingRoute"];
            }
            if (options["endLoadingRoute"] !== undefined) {
                this.endLoadingRoute = options["endLoadingRoute"];
            }
        }
        return RouteListener;
    }());
    exports["default"] = RouteListener;
});
//# sourceMappingURL=RouteListener.js.map