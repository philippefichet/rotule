export default class RouteListener {
    startLoadingRoute: Function = null
    endLoadingRoute: Function = null
    constructor(options) {
        if (options["startLoadingRoute"] !== undefined) {
            this.startLoadingRoute = options["startLoadingRoute"];
        }
        if (options["endLoadingRoute"] !== undefined) {
            this.endLoadingRoute = options["endLoadingRoute"];
        }
    }
}
